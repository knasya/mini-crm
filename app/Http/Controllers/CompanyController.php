<?php

namespace App\Http\Controllers;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNotify;
use Illuminate\Support\Facades\Storage;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $сompanies = Company::orderBy('name')->paginate(5);
        return view('сompanies.index', ['сompanies'=>$сompanies]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('сompanies.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => ['regex:/^[a-zA-Z0-9А-Яа-я\- ]{1,}$/u', 'required'],
            'email'=>['email', 'unique:users'],
            'logo'=>['image', 'dimensions:min_width=100,min_height=100'],
            'website' => ['active_url', 'url'],
        ]);

        if($request->hasFile('logo')){
            $logo = $request->file('logo')->store('', ['disk' => 'public']);
        } else {
            $logo = NULL;
        }

        Company::create([
        'name'=>$request->name,
        'email'=>$request->email,
        'logo'=>$logo ?? NULL,
        'website'=>$request->website,
        ]);

        $mailData = [
            'title' => config('gmail.mail_title'),
            'body' => config('gmail.mail_body').$request->name,
        ];

        Mail::to(config('gmail.mail_gmail'))->send(new MailNotify($mailData));

        return redirect('companies');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Company $company)
    {
        return view('сompanies.edit', ['company'=>$company]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Company $company)
    {

        $request->validate([
            'name' => ['regex:/^[a-zA-Z0-9А-Яа-я\- ]{1,}$/u', 'required'],
            'email'=>['email', 'unique:users'],
            'logo'=>['image', 'dimensions:min_width=100,min_height=100'],
            'website' => ['active_url', 'url'],
        ]);

        if($request->hasFile('logo')){
            $logo = $request->file('logo')->store('', ['disk' => 'public']);
            $company->logo = $logo;
        }
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->save();

        return redirect('companies');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Company $company)
    {
        if (Storage::disk('public')->exists($company->logo)) {
            Storage::disk('public')->delete($company->logo);
        }

        $company->delete();
        return redirect('companies')->with('info', 'Компания удалена');
    }
}