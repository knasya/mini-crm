<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeesStoreRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $employees = Employee::with(['companies'])->orderBy('name')->paginate(5);
        
        return view('employees.index', ['employees'=>$employees]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('employees.create', ['companies'=>Company::all()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EmployeesStoreRequest $request)
    {

        Employee::create([
        'name'=>$request->name,
        'surname'=>$request->surname,
        'company'=>$request->company,
        'email'=>$request->email,
        'phone'=>$request->phone,
        ]);

        return redirect('employees')->with('info', 'Товар добавлен');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit', ['employee'=>$employee, 'companies'=>Company::all()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EmployeesStoreRequest $request, Employee $employee)
    {
        $employee->name = $request->name;
        $employee->surname = $request->surname;
        $employee->company = $request->company;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->save();

        return redirect('employees');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return redirect('employees');
    }
}
