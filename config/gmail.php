<?php
return [
    'mail_title' => env('MAIL_TITLE'),
    'mail_body' => env('MAIL_BODY'),
    'mail_gmail' => env('MAIL_GMAIL'),
    'mail_name' => env('MAIL_NAME'),
    'mail_subject' => env('MAIL_SUBJECT'),
];