<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'name' => 'Анастасия',
                'email' => 'admin@admin.com',
                'password' => '$2y$12$6ESDhuDMG2px0FI9joUND.ib/lWs908b482SjXJ3Q76.joHPNYOoG',
                'created_at' => Now(),
                'updated_at' => Now(),
            ],
        ]);
    }
}
