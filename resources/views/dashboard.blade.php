<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Главная') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100 text-center mb-5 mt-5">
                            <h1 class="mb-3 text-9xl">Mini-CRM</h1>
                            <a class="btn btn-success text-lg" href="{{route('companies.index')}}" role="button">Компании</a>
                            <a class="btn btn-success text-lg" href="{{route('employees.index')}}" role="button">Сотрудники</a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
