<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Сотрудники') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
            
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                    <h3 class="mb-5 text-center">Добавление нового сотрудника</h3>
                    <form class="row g-3" method="post" action="{{route('employees.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-6">
                        <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="inputName" name="name" aria-describedy="name" placeholder="Имя" value="{{ old('name') }}" required>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control {{ $errors->has('surname') ? ' is-invalid' : '' }}" id="inputSurname" name="surname" aria-describedy="surname" placeholder="Фамилия" value="{{ old('surname') }}" required>
                    </div>
                    <div class="col-md-6">
                        <select class="form-select" aria-label="Кампания" name="company">
                        @foreach ($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                    <input type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="inputEmail" name="email" aria-describedy="email" placeholder="Email" value="{{ old('email') }}">
                    </div>
                    <div class="col-md-6">
                    <input type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="inputPhone" name="phone" aria-describedy="phone" placeholder="Телефон" value="{{ old('phone') }}">
                    </div>
                    <div class="col-12 text-center mt-5">
                        <button type="submit" class="btn btn-outline-success">Добавить</button>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>