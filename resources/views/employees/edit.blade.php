<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Сотрудники') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <h3 class="mb-5 text-center">Измение данных о сотруднике</h3>
                            <form class="row g-3" method="post" action="{{route('employees.update', $employee)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputName" name="name" aria-describedy="name" placeholder="Имя" value="{{$employee->name}}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputSurname" name="surname" aria-describedy="surname" placeholder="Фамилия" value="{{$employee->surname}}">
                            </div>
                            <div class="col-md-6">
                                <select class="form-select" aria-label="Категория товара" name="company">
                                @foreach ($companies as $company)
                                    <option value="{{$company->id}}" 
                                    @if ($company->id == $employee->company)
                                        selected
                                    @endif>
                                    {{$company->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputEmail" name="email" aria-describedy="email" placeholder="Email" value="{{$employee->email}}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputPhone" name="phone" aria-describedy="phone" placeholder="Телефон" value="{{$employee->phone}}">
                            </div>
                            <div class="col-12 text-center mt-5">
                                <button type="submit" class="btn btn-outline-success">Обновить</button>
                            </div>
                            </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>