<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Сотрудники') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    

                    <a class="btn btn-success mb-3" href="{{route('employees.create')}}" role="button">Добавить нового сотрудника</a>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">Имя</th>
                            <th scope="col">Фамилия</th>
                            <th scope="col">Компания</th>
                            <th scope="col">Email</th>
                            <th scope="col">Телефон</th>
                            <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($employees as $employee)
                            <tr>
                            <td>{{$employee->name}}</td>
                            <td>{{$employee->surname}}</td>
                            <td>{{$employee->companies->name}}</td>
                            <td>{{$employee->email}}</td>
                            <td>{{$employee->phone}}</td>
                            <td>
                            <a class="btn btn-light mb-1" href="{{route('employees.edit', $employee)}}" role="button">Изменить</a>
                            <br />
                            <form method="POST" action="{{route('employees.destroy', $employee)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-outline-danger" type="submit">Удалить</button>
                            </form>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>

                        <div>
                            {{ $employees->links() }}
                        </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
