<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Компании') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                            <h3 class="mb-5 text-center">Измение данных о компании</h3>
                            <form class="row g-3" method="post" action="{{route('companies.update', $company)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputName" name="name" aria-describedy="name" placeholder="Название" value="{{$company->name}}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputEmail" name="email" aria-describedy="email" placeholder="Email" value="{{$company->email}}">
                            </div>
                            <div class="col-md-6">
                                <img src="/img/{{$company->logo}}" alt="logo" width="100" height="100">
                                <input type="file" class="form-control" id="inputLogo" name="logo" aria-describedy="logo" enctype="multipart/form-data" placeholder="Путь к картинке">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputWebsite" name="website" aria-describedy="website" placeholder="Сайт" value="{{$company->website}}">
                            </div>
                            <div class="col-12 text-center mt-5">
                                <button type="submit" class="btn btn-outline-success">Обновить</button>
                            </div>
                            </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>