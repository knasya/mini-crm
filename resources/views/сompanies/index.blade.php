<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Компании') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    

                    <a class="btn btn-success mb-3" href="{{route('companies.create')}}" role="button">Добавить новую компанию</a>

                        <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">Имя</th>
                            <th scope="col">Логотип</th>
                            <th scope="col">Сайт</th>
                            <th scope="col">Email</th>
                            <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($сompanies as $company)
                            <tr>
                            <th>{{$company->name}}</th>
                            <td>
                            <img src="/img/{{$company->logo}}" height="100" width="100">
                            </td>
                            <td>{{$company->website}}</td>
                            <td>{{$company->email}}</td>
                            <td>
                            <a class="btn btn-light mb-1" href="{{route('companies.edit', $company)}}" role="button">Изменить</a>
                            <br />
                            <form method="POST" action="{{route('companies.destroy', $company)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-outline-danger" type="submit">Удалить</button>
                            </form>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>

                        <div>
                            {{ $сompanies->links() }}
                        </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
